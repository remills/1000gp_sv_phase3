#!/usr/bin/perl

use strict;
use warnings;
use Vcf;

my $batchNum = shift;
my $plink = "/home/remills/apps/plink-0.1.90/plink";
my $tabix = "/home/remills/apps/tabix-0.2.5/tabix";

my $fileSites = "/home/remills/Projects/thousandGenomes/phase3/ld/sites/ALL.autosomes.phase3_shapeit2_mvncall_integrated_v5.20130502.sites.svs.af0_1.vcf.gz";
my $dirBatch = "/home/remills/Projects/thousandGenomes/phase3/ld/randomSv3/$batchNum";
my $dirTmp = "/home/remills/scratch/tmp/remills/$batchNum";
my $dirGeno = "/home/remills/scratch/datasets/1000genomes/ftp/release/20130502";

my $fileSnps  = "/home/remills/Projects/thousandGenomes/phase3/ld/gwas_sv_ld_filt_af.txt";
my @chrs      = qw(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22);
my %chrLens   = (
    1  => 249250621,
    2  => 243199373,
    3  => 198022430,
    4  => 191154276,
    5  => 180915260,
    6  => 171115067,
    7  => 159138663,
    8  => 146364022,
    9  => 141213431,
    10 => 135534747,
    11 => 135006516,
    12 => 133851895,
    13 => 115169878,
    14 => 107349540,
    15 => 102531392,
    16 => 90354753,
    17 => 81195210,
    18 => 78077248,
    19 => 59128983,
    20 => 63025520,
    21 => 48129895,
    22 => 51304566
);

my %snps     = ();
#get GWAS SNPs for reference set
open( SNP, $fileSnps ) || die "Could not open $fileSnps for input, $!\n";

while (<SNP>) {
    if (/SNP_ID/) { next; }
    chomp;
    my @row = split(/\t/);
    my $snp = $row[0];
    $snps{$snp}{chr} = $row[1];
    $snps{$snp}{pos} = $row[2];
    my @afs = split( /,/, $row[20] );
    $snps{$snp}{af} = sprintf( "%0.1f", ( sort { $b <=> $a } @afs )[0] );
}
close SNP;

my $w      = 1000000;

mkdir($dirBatch);
mkdir($dirTmp);

if (! -e "$dirBatch/random.txt") { exit; } 

open( IN, "$dirBatch/random.txt" ) || die "Cannot open random.txt file for input, $!\n";
while (<IN>) {
    if (/^id/) { next; } 
    chomp;
    my ($id, $chr, $pos) = split(/\t/);
    if (-e "$dirBatch/$id.ld.gz") { next; }  #file aleady exists so we cool
    my $fileRandVcfGeno = "$dirGeno/ALL.chr$chr.phase3_shapeit2_mvncall_integrated_v5.20130502.genotypes.vcf.gz";
    my $left = $pos - $w;
    my $right = $pos + $w;
    if ($left < 0) { $left = 1; $right = 2 * $w; }

    open(VCFO, ">$dirTmp/$id.vcf");
    open(VCFI, "$tabix -h $fileRandVcfGeno $chr:$left-$right |");
    while (<VCFI>) {
        if (/^#/) { print VCFO; next; } 
        chomp;
        my $snp = ( split /\t/, $_, 8 )[ 2 ];
        if (defined($snps{$snp}) || $snp eq $id) { print VCFO "$_\n"; }  
    }
    close VCFI;
    close VCFO;
    
    #LD analysis
    `$plink --vcf-half-call 'haploid' --vcf $dirTmp/$id.vcf --out $dirTmp/$id`;
    `$plink --bfile $dirTmp/$id --r2 'gz' --ld-snp $id --ld-window-r2 0.0 --ld-window 100000 --out $dirBatch/$id --threads 1 --memory 4000`;
    `rm $dirTmp/$id*`;
}
close IN;

