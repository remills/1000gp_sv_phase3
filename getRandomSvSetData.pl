#!/usr/bin/perl

use strict;
use warnings;
use IO::File;

my %stats = ();
my %f     = ();

my $batch       = shift;
my $dirRandom   = "/home/remills/Projects/thousandGenomes/phase3/ld/randomSv3/$batch";
my $dirObs      = "/home/remills/Projects/thousandGenomes/phase3/ld/randomSv3/obs";
my $tabix       = "/home/remills/apps/tabix-0.2.5/tabix";
my $fileVcfGeno = "/home/remills/scratch/datasets/1000genomes/ftp/technical/working/20130723_phase3_wg/merged_sv_genotypes/ALL.wgs.mergedSV.v3.20130502.svs.genotypes.vcf.gz";
my @sizeBins = qw(1000 5000 20000);
my $winLen = 1000000;

my @svtypes = qw(MAX DEL DUP CNV INV ALU LINE1 SVA INS);

if (! -e "$dirRandom/random.txt") { die "random.txt does not exist for batch $batch, skipping\n"; }
my $cntRandom = `wc -l $dirRandom/random.txt`;
my @c = split(/\s+/, $cntRandom);
if ($c[0] < 4000) { die "Not enough counts to be considered at this point\n"; }

my %obs = ();
open( OBS, "$dirObs/random.txt");
while (<OBS>) {
    if (/^id/)  {next; }
    chomp;
    my ($id, $chr, $pos, $end, $svlen, $svtype, $af, $blockLen) = split(/\t/);
    $obs{$id}{svlen} = abs($svlen);
    $obs{$id}{svtype} = $svtype;
}
close OBS;

my $n = 0;

open( RAND, "$dirRandom/random.txt" ) || die "could not open $dirRandom/random.txt for input, $!\n";
while (<RAND>) {
    chomp;
    my ( $snp, $chr, $pos, $matchedId, $af, $blockLen ) = split(/\t/);

    if ( !-e "$dirRandom/$snp.ld.gz" ) { next; }
    open( IN, "zcat $dirRandom/$snp.ld.gz |" ) || die "$!\n";

    if (defined($obs{$snp})) { $matchedId = $snp; }  #for observed set
    my $svtype = $obs{$matchedId}{svtype};
    my $svlen = $obs{$matchedId}{svlen};
    #bin by size - update 010715 to look at more bins
    #my $logsvlen = int(log10($svlen));
    my $logsvlen = 100;
    foreach my $sizeBin (@sizeBins) {
        if ($svlen >= $sizeBin) { $logsvlen = $sizeBin; } 
    }
     
    my $maxR2 = 0;

    while (<IN>) {
        if (/CHR_A/) { next; }
        chomp;
        my ( $dummy, $chr_a, $bp_a, $snp_a, $chr_b, $bp_b, $snp_b, $r2 ) = split(/\s+/);
        if ( $snp_b eq $snp ) { next; }    #don't count SNP itself
        if ( $r2 > $maxR2 ) { $maxR2 = $r2; }
    }
    close IN;
    for (my $i=0; $i<=0.80; $i+=0.2) {
        if ($maxR2 >= $i) {
            $stats{MAX}{$logsvlen}{$i}++;
            $stats{$svtype}{$logsvlen}{$i}++;
        }
    }
     
}
open(OUT, ">$dirRandom/stats_010815b.txt");
print OUT "svtype\tlen\tr2\tcnt\n";
foreach my $svtype (keys %stats) {
    foreach my $svlen (@sizeBins) {
        for (my $i=0; $i<=0.8; $i+=0.2) {
            if (defined($stats{$svtype}{$svlen}{$i})) { print OUT "$svtype\t$svlen\t$i\t$stats{$svtype}{$svlen}{$i}\n"; }
            else { print OUT "$svtype\t$svlen\t$i\t0\n"; }
        }
    }
}
close OUT;



sub log10 {
    my $n = shift;
    return log($n) / log(10);
}
