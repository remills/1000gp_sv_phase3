#!/usr/bin/perl

use strict;
use warnings;
use Vcf;

my $batch = shift;
my $plink = "/home2/remills/apps/plink-0.1.90/plink";
my $tabix = "/home2/remills/apps/tabix-0.2.5/tabix";

my $fileSites = "/scratch/remills_flux/remills/phase3/ld/sites/ALL.autosomes.phase3_shapeit2_mvncall_integrated_v5.20130502.sites.svs.af0_1.vcf";
my $dirGeno   = "/home2/remills/scratch/datasets/1000genomes/ftp/release/20130502";
#my $dirTmp    = "/scratch/remills_flux/remills/phase3/ld/tmpSv2/$batch";
my $dirTmp = "/tmp/remills_flux/$batch";
my $dirBatch  = "/scratch/remills_flux/remills/phase3/ld/randomSv3/$batch";
my $dirObs    = "/scratch/remills_flux/remills/phase3/ld/randomSv3/obs";

my @chrs      = qw(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22);
my %chrLens   = (
    1  => 249250621,
    2  => 243199373,
    3  => 198022430,
    4  => 191154276,
    5  => 180915260,
    6  => 171115067,
    7  => 159138663,
    8  => 146364022,
    9  => 141213431,
    10 => 135534747,
    11 => 135006516,
    12 => 133851895,
    13 => 115169878,
    14 => 107349540,
    15 => 102531392,
    16 => 90354753,
    17 => 81195210,
    18 => 78077248,
    19 => 59128983,
    20 => 63025520,
    21 => 48129895,
    22 => 51304566
);

my $w     = 1000000;
my $n     = 0;
my $t     = 0;
my %obs   = ();
my %af    = ();
my $index = 0;
my $blockFrac = 0.50;


for ( my $i = 0 ; $i <= 1 ; $i += 0.1 ) {
    $i = sprintf("%0.1f", $i);
    $af{$i}{t} = 0;
    $af{$i}{n} = 0;
}

mkdir("/tmp/remills_flux");
mkdir($dirTmp);
mkdir($dirBatch);

#figure out what we need to match to (af, blocksize)
open( IN, "$dirObs/random.txt" );
while (<IN>) {
    if (/id/) { next; }
    chomp;
    $index++;
    my ( $id, $chr, $pos, $end, $svlen, $svtype, $af, $blockLen ) = split(/\t/);
    my %hash = (id => $id, blockLen => $blockLen);
    push @{ $obs{$af} }, \%hash; 
    $af{$af}{t}++;
    $t++;
}
close IN;

#continue from existing set, if available
if (-e "$dirBatch/random.txt") {
    `cp $dirBatch/random.txt $dirBatch/random.bak`;
    open (RAND, "$dirBatch/random.txt");
    while (<RAND>) {
        if (/^id/) { next; } 
        my ($id, $chr, $pos, $afr, $blockLen) = split(/\t/);
        for (my $i=0; $i<=$#{$obs{$afr}}; $i++) {
            my $obsLen = $obs{$afr}[$i]{blockLen};
            if (($obsLen <= $blockLen && $obsLen/$blockLen >= $blockFrac) || ($obsLen > $blockLen && $blockLen/$obsLen >= $blockFrac)) {
                $n++;
                $af{$afr}{n}++;
                $obs{$afr}[$i]{blockLen} = -1; #zero out this block size, may want to slice out later for efficiency 
                last;
            }
        } 
    }
    close RAND;
    open( OUT, ">>$dirBatch/random.txt" );
}
else {
    open( OUT, ">>$dirBatch/random.txt" );
    print OUT "id\tchr\tpos\tmatchedId\taf\tblockLen\n";
}

my $iterations    = 0;
my $maxIterations = 10000;

while ( $n < $t && $iterations < $maxIterations ) {
    $iterations++;

    my $chr = $chrs[ rand @chrs ];
    my $pos = int( rand( $chrLens{$chr} ) ) + 1;

    #estimate haplotype size using SNPs only
    my $left  = $pos - $w;
    my $right = $pos + $w;
    if ( $left < 0 ) { $left = 1; $right = 2 * $w; }    #offset window for edge effect

    #find random SNP at location
    my $fileGeno = "$dirGeno/ALL.chr$chr.phase3_shapeit2_mvncall_integrated_v5.20130502.genotypes.vcf.gz";
    open( TAB, "tabix $fileGeno $chr:$left-$right | cut -f 1-8 |" );

    my $id = "";
    while (<TAB>) {
        chomp;
        my @row = split(/\t/);
        my $snp = $row[2];
        if ( $snp !~ /rs/ || $snp eq "." ) { next; }    #only want SNPs!
        my $chrSnp = $row[0];
        my $posSnp = $row[1];
        if ( $snp eq "." ) {
            $snp = "$chrSnp\_$posSnp";
        }
        my ($afVcf) = $row[7] =~ /;AF=(\S+?);/;
        my @afs = split( /,/, $afVcf );
        my $afr = sprintf( "%0.1f", ( sort { $b <=> $a } @afs )[0] );
        if ( $af{$afr}{n} == $af{$afr}{t} ) { next; }    #already filled this bin

        #check haplotype block size
        $left = $posSnp - $w; #recenter on SNP
        $right = $posSnp + $w;
        if ( $left < 0 ) { $left = 1; $right = 2 * $w; }    #offset window for edge effect

        `$tabix -h $fileGeno $chrSnp:$left-$right > $dirTmp/$snp.vcf`;
        `$plink --vcf-half-call 'haploid' --vcf $dirTmp/$snp.vcf --out $dirTmp/$snp`;
        `$plink --bfile $dirTmp/$snp --ld-snp $snp --r2 'gz' --ld-window-r2 0.8 --ld-window 100000 --out $dirTmp/$snp`;

        my $blockStart = 0;
        my $blockEnd   = 0;
        my $lineNum    = 0;

        open( HAP, "zcat $dirTmp/$snp.ld.gz |" );
        while (<HAP>) {
            if (/SNP_A/) { next; }
            chomp;
            $lineNum++;
            my ( $dummy, $chr_a, $bp_a, $snp_a, $chr_b, $bp_b, $snp_b, $r2 ) = split(/\s+/);
            if   ( $lineNum == 1 ) { $blockStart = $bp_b; }
            else                   { $blockEnd   = $bp_b; }
        }
        close HAP;
        my $blockLen = "NA";
        if ( $blockStart > 0 && $blockEnd > 0 ) {
            $blockLen = $blockEnd - $blockStart + 1;
        }
        if ($blockLen eq "NA") { 
            #`rm $dirBatch/$snp*`; 
            `rm $dirTmp/$snp*`; 
            next; 
        }

        my $found = 0;
        for (my $i=0; $i<=$#{$obs{$afr}}; $i++) {
            my $obsLen = $obs{$afr}[$i]{blockLen};
            if (($obsLen <= $blockLen && $obsLen/$blockLen >= $blockFrac) || ($obsLen > $blockLen && $blockLen/$obsLen >= $blockFrac)) {
                $found = 1;
                $n++;            
                $af{$afr}{n}++;
                $obs{$afr}[$i]{blockLen} = -1; #zero out this block size, may want to slice out later for efficiency 
                print OUT "$snp\t$chrSnp\t$posSnp\t$obs{$afr}[$i]{id}\t$afr\t$blockLen\n";
                last;
            }
        }
        #clean up so HPC guys don't get mad at me again :(
        #`rm $dirBatch/$snp*`; 
        `rm $dirTmp/$snp*`;
        if ($found) { last; }  
    }
}
close OUT;
