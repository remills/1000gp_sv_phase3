# GWAS SNP and SV Enrichment Analysis #

The following files were used to generate and analyze the relative enrichment of GWAS SNPs in the flanking regions of SVs:

* gwas_sv_ld_filt_af.txt - set of GWAS SNPs from the NHGRI GWAS catalog (http://www.genome.gov/gwastudies/) that are also present in the 1000GP Phase 3 release set, filtered to only include a single representative variant for redundant SNPs in high LD (>0.80 with each other and only associated with the same phenotype
* ALL.autosomes.phase3_shapeit2_mvncall_integrated_v5.20130502.sites.svs.af0_1.vcf.gz (filtered SV MAF >0.01 derived from: ftp://ftp-trace.ncbi.nih.gov/1000genomes/ftp/release/20130502/ALL.wgs.phase3_shapeit2_mvncall_integrated_v5.20130502.sites.vcf.gz)
* observed.txt - filtered set of SVs with denoted svlen, allele frequency, svtype, and haplotype block length
* generateRandomSvSet.pl - Using the set of observed SVs, generates an equivalent random set by matching allele frequency (to the nearest 0.1) and haplotype size (within 50% length of each other) to a SNP elsewhere in the genome
* getLdWindowsFromRandomSvSet.pl - Given a randomly generated set, reports the r2 of all GWAS SNPs within a 1Mbp window around each variant
* getRandomSvSetData.pl - summarizes all random sets by SV type, length, and LD
* plotRandomSvSetData.R - R script for plotting the fold enrichment of the summarized random data against the observed data

Other files that are utilized and not included:

* ftp://ftp-trace.ncbi.nih.gov/1000genomes/ftp/technical/working/20130723_phase3_wg/merged_sv_genotypes/ALL.wgs.mergedSV.v2.20130502.svs.genotypes.vcf.gz
* ftp://ftp-trace.ncbi.nih.gov/1000genomes/ftp/release/20130502/ (SNP genotypes)